About:

    Web Application for immediate verification of status (Enrolled or Not Enrolled) of an institution transport facility users and maintenance of the user’s status.

Database Creation:

    Import the "busfees.sql" file inside the Database folder which will create the database.

Admin Login:

    User ID         :admin
    Password       :password

User Login:

    User ID     :user1
    Password    :pswd
