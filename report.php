<?php
	session_start();
	$servername = "localhost";
	$username = "BusFees";
	$password = "password";
	$dbname = "busfees";

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	if (!$conn) {
	    die("Connection failed: " . mysqli_connect_error());
	}


	$a=$_SESSION['rep_date'];
	//$a="2019-12-10";
	echo $_SESSION['rep_date']."<br>";
	if($_SESSION['user_id']=='admin')
	{
	    
		$sql = " SELECT * FROM `report` WHERE date='$a' ";

		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) >0) 
		{

			while($row = mysqli_fetch_assoc($result)) 
			{
				echo $row["date"] . " - " . $row["name"] . " -" . $row["rollno"] . "-" . $row["college"] . "-" ."department" . "-" . $row["area"] . "<br>";

        		//echo $row["date"]. " - " . $row["name"]. " -" . $row["rollno"]."-". $row["college"]."-"."department."-"." $row["area"]".<br>";
    		}			
		} 	
	
	}	

?>


<html>
<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>TMS Report</title>
	</head>
<body>
   <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TMS Report</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"></ul>
      <form class="form-inline my-2 my-lg-0">
        <button class="btn btn-outline-success my-2 my-sm-0">Contact Us</button>
      </form>
    </div>
  </nav>


		<div class="container-sm" style="margin-top: 10%;">
		<div class="input-group mb-3">
  <input type="text" class="form-control" placeholder="Enter date in dd/mm/yyyy" id="date">
  <div class="input-group-append">
    <a class="btn btn-outline-danger" type="button" id="button-addon2" id="disp">Button</a>
  </div>
</div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
				<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


	<script>
		$(document).ready(function()
			{
				  $("#disp").click(function()
				  {
				  		var date=$('#date').val();		                               
		                  $.ajax({
		                        type: "POST",
		                        url: 'selreport.php',
		                        data : {
		                            date:date,
		                        },
		                        success: function(data)
		                        {
		                            if(data==1)
		                            {                                                                
		                                window.location="report.php";
		                            }  	                                             
		                        }
		                    });		              		             
							});
							$("#logout").click(function()
	                {
	                     window.location="logout.php";                                		             
	                });  
	      })
	</script>

</body>

</html>